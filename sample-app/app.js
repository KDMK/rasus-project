const express = require('express')
const fs = require('fs')
const app = express()

const port = 80 

app.get('/', (request, response) => {
  response.send(`Hello from Express! It\'s running on ${request.hostname}`)
})

app.get('/big', (request, response) => {
  let json = fs.readFileSync('big.json', 'UTF-8')

  response.send(json)
})

app.listen(port, '0.0.0.0', (err) => {
  if (err) {
    return console.log('Error!', err)
  }

  console.log(`Server is listening on ${port}`)
})

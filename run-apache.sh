#!/bin/bash

docker build -t sample-app:latest ./sample-app
docker build -t locust-app:latest ./locust-testing
docker-compose -f ./apache/docker-compose.yml up -d --build
